###################################################
##
## Remotely connect and verify FIPS is Enabled 
## For Python 2.7
## Requires paramiko library
##
###################################################

#import os and paramiko libraries
import os
from hashlib import sha256
import paramiko
from paramiko import PKey
import getpass

# output paramiko commands to logfile
paramiko.util.log_to_file('/tmp/ssh.log')

# setup patch to use hsa255 key as MD5 fails in paramiko

def _patched_get_fingerprint(self):
    """Return an MD5 or SHA-256 fingerprint of the public part of this key.
    Nothing secret is revealed.

    This method is monkey patched on paramiko.Pkey.get_fingerprint method to
    handle missing MD5 on FIPS-enabled system.

    :return: a 16-byte `string <str>` (binary) of the MD5 fingerprint or a
        32-byte `string <str>` (binary) of the SHA-256 fingerprint if MD5 is
        disabled, in SSH format.
    """
    try:
        return _get_fingerprint(self)
    except ValueError:
        return sha256(self.asbytes()).digest()


_get_fingerprint = PKey.get_fingerprint
# Patches original method to handle missing MD5 on FIPS-enabled system.
PKey.get_fingerprint = _patched_get_fingerprint

# Get username and password from stdin
varuser=raw_input("Enter username: ")
varpass=getpass.getpass(prompt='Enter password: ')

# delete output file if exists
if os.path.exists("./out.csv") :
  os.remove("./out.csv")


# Setup client and run verify script on all hosts in hosts.txt
with open("./hosts.txt", "r") as infile:
   with open("./out.csv", "a") as outfile:
     for i in infile:
       host=i.strip()
       client = paramiko.SSHClient()
       #client.load_system_host_keys()
       client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
       client.connect(hostname=host,username=varuser,password=varpass)
       client.stdin, client.stdout, client.stderr = client.exec_command('cat /proc/sys/crypto/fips_enabled')
       # DEBUG
       # print("client.stdout")
       # Print return information
       outfile.write(host+","+client.stdout.read())
       # close the ssh connection. If not closed, will cause the session to
       # remain open indefinitely
       client.close()
