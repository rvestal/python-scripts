# python-scripts

My repository of python scripts I've used in my career. Feel free to use them at no charge, but please give me credit for the work.</br>
***Please Note - most of these will be python3***

* paramiko-ssh.py - A simple ssh script using paramiko to execute a script on a remote server
* fips-verify.py - A script to ssh and verify if fips is enabled using hosts.txt as input
