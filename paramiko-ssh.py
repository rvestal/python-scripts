# Uses paramiko to ssh to a server and run a command
# NOTE - This stores the password in plain text in this script
# Check paramiko documentation for more information

#import os and paramiko libraries
import os
import paramiko

# output paramiko commands to logfile
paramiko.util.log_to_file('ssh.log')

#setup paramiko client
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
#client.load_system_host_keys()
client.connect(hostname='<hostname>', username="<username>", password="<password>")
client.stdin, client.stdout, client.stderr = client.exec_command('hostname')


# DEBUG
# print("client.stdout")

# Print return information
print(client.stdout.read())

# close the ssh connection. If not closed, will cause the session to
# remain open indefinitely
client.close()
